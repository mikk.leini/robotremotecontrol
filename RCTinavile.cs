﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Drawing;
using System.Windows.Forms;

namespace RobotRemoteControl
{
    class RCTinavile : RC
    {
        enum ReceiveState
        {
            WaitPreambula,
            WaitID,
            WaitLength,
            WaitData,
            WaitChecksum
        };

        private ReceiveState receiveState = ReceiveState.WaitPreambula;
        private int receiveID, receiveLength, receiveIndex, receiveChecksum;
        private byte[] receiveData = new byte[256];       
        private double dLeft, dRight, dAccel;
        private short realSpeedFL, realSpeedFR, realSpeedRL, realSpeedRR;
        private uint lineStatus;
        private uint obstacleStatus;

        //
        // Name
        //
        override public string Name
        {
            get { return "Tinavile specific drive"; }
        }       

        //
        // Constructor
        //
        public RCTinavile()
        {
            if (!BitConverter.IsLittleEndian)
            {
                MessageBox.Show("Software not operating on little-endian architecture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //
        // Command sending
        //        
        private void SendSpeedControl(short left, short right)     
        {
            byte[] data = new byte[4];

            // Pack words in little-endian
            Array.Copy(BitConverter.GetBytes(left), 0, data, 0, 2);
            Array.Copy(BitConverter.GetBytes(right), 0, data, 2, 2);
            
            SendMessage(0x01, 4, data);
        }

        //
        // Control
        //
        override public void Control(ControlValues values)
        {
            byte[] parameters = new byte[10];
            short sLeft, sRight, sAccel;

            // Convert from X/Y to differential
            dLeft = Limit(values.Y + values.X, -1, 1);
            dRight = Limit(values.Y - values.X, -1, 1);
            dAccel = Limit(values.Acceleration, 0, 1);

            // Convert to 7 bit binary
            sLeft = (short)(dLeft * 12000);
            sRight = (short)(dRight * 12000);
            sAccel = (short)(dAccel * 65535);

            // Send speed control command
            SendSpeedControl(sLeft, sRight);

            // Space press ?
            if ((values.Flags & 0x01) > 0)
            {
                SendMessage(0x02, 1, new byte[] { 0x01 });
            }
        }

        //
        // Paint indicators
        //
        override public void Paint(Graphics gfx, Point quad)
        {
            float rsl = (float)realSpeedFL / 12000.0f;
            float rsr = (float)realSpeedFR / 12000.0f;
            int sl = (int)(rsl * quad.Y);
            int sr = (int)(rsr * quad.Y);
            
            int cl = (int)(dLeft * 127);
            int cr = (int)(dRight * 127);

            Color colL = Color.FromArgb(255, 127 - cl, 0, 127 + cl);
            Color colR = Color.FromArgb(255, 127 - cr, 0, 127 + cr);  

            // Draw speed bars
            gfx.DrawLine(new Pen(CreateSpeedColor(rsl), 5), new Point(-quad.X + 3, 0), new Point(-quad.X + 3, -sl));
            gfx.DrawLine(new Pen(CreateSpeedColor(rsr), 5), new Point(+quad.X - 3, 0), new Point(+quad.X - 3, -sr));

            // Draw robot border
            gfx.DrawRectangle(Pens.Black, -60, -60, 120, 120);

            // Draw line sensors            
            DrawLineSensor(gfx, new Point(- 30, - 40), ((lineStatus & (1 << 0)) > 0));
            DrawLineSensor(gfx, new Point(+ 30, - 40), ((lineStatus & (1 << 4)) > 0));
            DrawLineSensor(gfx, new Point(- 48, - 55), ((lineStatus & (1 << 1)) > 0)); // A
            DrawLineSensor(gfx, new Point(+ 48, - 55), ((lineStatus & (1 << 5)) > 0)); // A
            DrawLineSensor(gfx, new Point(- 55, - 55), ((lineStatus & (1 << 2)) > 0)); // B
            DrawLineSensor(gfx, new Point(+ 55, - 55), ((lineStatus & (1 << 6)) > 0)); // B
            DrawLineSensor(gfx, new Point(- 55, - 48), ((lineStatus & (1 << 3)) > 0)); // C
            DrawLineSensor(gfx, new Point(+ 55, - 48), ((lineStatus & (1 << 7)) > 0)); // 

            // Draw obstacle sensors
            DrawObstacleSensor(gfx, new Point(- 30,  - 60), new Point(0, -30), ((obstacleStatus & (1 << 0)) > 0));
            DrawObstacleSensor(gfx, new Point(+ 30,  - 60), new Point(0, -30), ((obstacleStatus & (1 << 1)) > 0));
            DrawObstacleSensor(gfx, new Point(- 57,  - 60), new Point(-5, -25), ((obstacleStatus & (1 << 2)) > 0));
            DrawObstacleSensor(gfx, new Point(+ 57,  - 60), new Point( 5, -25), ((obstacleStatus & (1 << 3)) > 0));
            DrawObstacleSensor(gfx, new Point(- 60,  - 55), new Point(-30, 0), ((obstacleStatus & (1 << 4)) > 0));
            DrawObstacleSensor(gfx, new Point(+ 60,  - 55), new Point( 30, 0), ((obstacleStatus & (1 << 5)) > 0));
            DrawObstacleSensor(gfx, new Point(- 60, 0), new Point(-30, 0), ((obstacleStatus & (1 << 6)) > 0));
            DrawObstacleSensor(gfx, new Point(+ 60, 0), new Point( 30, 0), ((obstacleStatus & (1 << 7)) > 0));
            DrawObstacleSensor(gfx, new Point(- 55,  + 60), new Point(0, 30), ((obstacleStatus & (1 << 8)) > 0));
            DrawObstacleSensor(gfx, new Point(+ 55,  + 60), new Point(0, 30), ((obstacleStatus & (1 << 9)) > 0));            
        }

        private void DrawLineSensor(Graphics gfx, Point pos, bool set)
        {
            SolidBrush brush = new SolidBrush(set ? Color.Red : Color.FromArgb(60, 0, 0));
            
            Rectangle rect = new Rectangle(pos.X - 3, pos.Y - 3, 6, 6);

            gfx.FillRectangle(brush, rect);
        }

        private void DrawObstacleSensor(Graphics gfx, Point pos, Point dir, bool set)
        {
            Pen pen = new Pen(Color.Red, 2.0f);
            if (!set)
            {
                gfx.DrawLine(pen, pos.X, pos.Y, pos.X + dir.X, pos.Y + dir.Y);
            }
            else
            {
                gfx.DrawLine(pen, pos.X, pos.Y, pos.X + dir.X * 0.1f, pos.Y + dir.Y * 0.1f);
            }
        }

        //
        // Message sending
        //
        protected void SendMessage(byte id, byte length, byte[] data)
        {
            byte[] buffer = new byte[16];
            int checksum = 0;

            // Form packet
            buffer[0] = 0xAA;
            buffer[1] = id;
            buffer[2] = length;

            // Calculate header checksum
            checksum = buffer[0] + buffer[1] + buffer[2];
            
            for (int i = 0; i < length; i++)
            {
                buffer[3 + i] = data[i];
                checksum += data[i];
            }

            // Checksum            
            buffer[3 + length] = (byte)(checksum & 0xFF);

            // Write to send buffer
            Send(buffer, 4 + length);

            //LogMessage(BinaryToHexString(buffer, 4 + length));
        }

        //
        // Data reception event
        //
        override public void DataReceived(byte[] buffer)
        {
            // Parse all bytes
            foreach (byte b in buffer)
            {
                switch (receiveState)
                {
                    case ReceiveState.WaitPreambula:
                        if (b == 0xAA)
                        {
                            receiveState = ReceiveState.WaitID;
                            receiveIndex = 0;
                            receiveChecksum = b;
                        }
                        else
                        {
                            // Unexpected byte...
                        }
                        break;

                    case ReceiveState.WaitID:
                        receiveID = b;
                        receiveChecksum += b;
                        receiveState = ReceiveState.WaitLength;
                        break;

                    case ReceiveState.WaitLength:
                        receiveLength = b;
                        receiveChecksum += b;
                        receiveState = ReceiveState.WaitData;
                        break;

                    case ReceiveState.WaitData:
                        receiveData[receiveIndex++] = b;
                        receiveChecksum += b;

                        // Got last byte ?
                        if (receiveIndex >= receiveLength)
                        {
                            receiveState = ReceiveState.WaitChecksum;
                        }
                        break;

                    case ReceiveState.WaitChecksum:
                        receiveChecksum = receiveChecksum & 0xFF;

                        // Check checksum
                        if (b == receiveChecksum)
                        {
                            // Correct message
                            HandleMessage(receiveID, receiveData, receiveLength);
                        }

                        receiveState = ReceiveState.WaitPreambula;
                        break;
                }
            }
        }

        //
        // Message handler
        //
        protected void HandleMessage(int id, byte[] buffer, int length)
        {
            LogMessage(id.ToString() + " = " + BitConverter.ToString(buffer, 0, length));

            switch (id)
            {
                case 0x03:                    
                    if (length == 8)
                    {
                        realSpeedFL = (short)-BitConverter.ToInt16(buffer, 0);
                        realSpeedFR = BitConverter.ToInt16(buffer, 2);
                        realSpeedRL = (short)-BitConverter.ToInt16(buffer, 4);
                        realSpeedRR = BitConverter.ToInt16(buffer, 6);
                    }                    
                    break;

                case 0x0A:
                    if (length == 1)
                    {                        
                        lineStatus = buffer[0];
                    }
                    break;

                case 0x05:                    
                    if (length == 2)
                    {
                        obstacleStatus = BitConverter.ToUInt16(buffer, 0);
                    }
                    break;
            }
        }
    }
}
