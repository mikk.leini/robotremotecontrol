﻿//
// Sumorobot remote control software
// 
// 
// Copyright
// TUT Robotics Club
// Mikk Leini
// 2009-2018
// 
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO.Ports;

namespace RobotRemoteControl
{
	public partial class MainForm : Form
	{
        private RC activeRemote = null;
        private List<RC> remotes = new List<RC>();
		private List<Keys> downKeys = new List<Keys>();        
        private Vector3 currentSpeed, destinationSpeed;
        private ControlValues values;
		private DateTime lastUpdate;
        private bool working = false;
        public delegate void DisplayMessageDelegate(string message);        
	
        //
        // Constructor
        //
		public MainForm()
		{
            currentSpeed = new Vector3();
            destinationSpeed = new Vector3();            
		            
            // Initialize form components
			InitializeComponent();

            // Display program version
            Text += " v" + Application.ProductVersion.TrimEnd(new char[] { '0', '.' });            

            // Folded size at first
            Width = MinimumSize.Width;
			
            // Set port reception event
            SerialPort.DataReceived += new SerialDataReceivedEventHandler(SerialDataReceived);

            // Fill parity enum
            ParitySelection.Items.AddRange(Enum.GetValues(typeof(Parity)).Cast<object>().ToArray());
            ParitySelection.SelectedIndex = 0;

            // Default baudrate	
            BaudRateSelection.SelectedIndex = 8;

            // Create port list
            RefreshPortList();

            // Create RC list
            MakeRCList();

            // Load settings
            LoadSettings();
		}

        //
        // Settings saving
        //
        private void SaveSettings()
        {
            int i;

            if (Int32.TryParse((string)BaudRateSelection.SelectedItem, out i))
            {
                Properties.Settings.Default.Baudrate = i;
            }

            Properties.Settings.Default.Port = (string)PortSelection.SelectedItem;
            Properties.Settings.Default.Parity = ParitySelection.SelectedItem.ToString();
            Properties.Settings.Default.RC = ControlMethod.SelectedIndex;
            Properties.Settings.Default.UpdateInterval = (int)UpdateInterval.Value;
            Properties.Settings.Default.MaxSpeed = (int)TopSpeed.Value;
            Properties.Settings.Default.Acceleration = (int)Acceleration.Value;
            Properties.Settings.Default.WindowWidth = Width;
            Properties.Settings.Default.WindowHeight = Height;
            Properties.Settings.Default.LogVisible = LogVisible;
            Properties.Settings.Default.LogSplitterDistance = listBox.Width;
            Properties.Settings.Default.Save();
        }

        //
        // Settings loading
        //
        private void LoadSettings()
        {
            for (int i = 0; i < BaudRateSelection.Items.Count; i++)
            {
                if ((string)BaudRateSelection.Items[i] == Properties.Settings.Default.Baudrate.ToString())
                {
                    BaudRateSelection.SelectedIndex = i;
                    break;
                }
            }

            for (int i = 0; i < PortSelection.Items.Count; i++)
            {
                if ((string)PortSelection.Items[i] == Properties.Settings.Default.Port)
                {
                    PortSelection.SelectedIndex = i;
                    break;
                }
            }

            for (int i = 0; i < ParitySelection.Items.Count; i++)
            {
                if (ParitySelection.Items[i].ToString() == Properties.Settings.Default.Parity)
                {
                    ParitySelection.SelectedIndex = i;
                    break;
                }
            }

            if ((Properties.Settings.Default.RC >= 0) && (Properties.Settings.Default.RC < ControlMethod.Items.Count))
            {
                ControlMethod.SelectedIndex = Properties.Settings.Default.RC;
            }

            if ((Properties.Settings.Default.UpdateInterval >= UpdateInterval.Minimum) && (Properties.Settings.Default.UpdateInterval <= UpdateInterval.Maximum))
            {
                UpdateInterval.Value = Properties.Settings.Default.UpdateInterval;
            }

            if ((Properties.Settings.Default.MaxSpeed >= TopSpeed.Minimum) && (Properties.Settings.Default.MaxSpeed <= TopSpeed.Maximum))
            {
                TopSpeed.Value = Properties.Settings.Default.MaxSpeed;
            }

            if ((Properties.Settings.Default.Acceleration >= Acceleration.Minimum) && (Properties.Settings.Default.Acceleration <= Acceleration.Maximum))
            {
                Acceleration.Value = Properties.Settings.Default.Acceleration;
            }

            Width = Properties.Settings.Default.WindowWidth;
            Height = Properties.Settings.Default.WindowHeight;

            LogVisible = Properties.Settings.Default.LogVisible;
            listBox.Width = Properties.Settings.Default.LogSplitterDistance;
        }


        //
        // Check that type origins from Module class
        //
        private bool CheckRootType(Type type, Type rootType)
        {
            Type currentType = type;
            Boolean result = false;

            // Find base class
            if (!currentType.IsAbstract)
            {
                while ((currentType.IsClass) && (currentType.BaseType != null))
                {
                    if (currentType.BaseType == rootType)
                    {
                        result = true;
                        break;
                    }

                    currentType = currentType.BaseType;
                }
            }

            return result;
        }

        //
        // RC list creation
        //
        private void MakeRCList()
        {
            // Find all classes that inherit from RC class
            foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type t in a.GetTypes())
                {
                    if (CheckRootType(t, typeof(RC)))
                    {
                        RC rc = (RC)Activator.CreateInstance(t);                        
                        remotes.Add(rc);
                    }
                }
            }

            // Fill combo box
            ControlMethod.Items.Clear();
            foreach (RC item in remotes)
            {
                ControlMethod.Items.Add(item.Name);
            }

            // Select first method
            ControlMethod.SelectedIndex = 0;
        }

        //
        // Port list updating
        //
        private void RefreshPortList()
        {
            string[] ports;
            int i;

            PortSelection.Items.Clear();
            ports = System.IO.Ports.SerialPort.GetPortNames();

            foreach (string port in ports)
            {
                PortSelection.Items.Add(port);
            }

            // Select current
            i = PortSelection.Items.IndexOf(SerialPort.PortName);
            if (i >= 0)
            {
                PortSelection.SelectedIndex = i;
            }
            else if (PortSelection.Items.Count > 0)
            {
                PortSelection.SelectedIndex = 0;
            }
        }

        // 
        // RC selection
        //
        private void ControlMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            PlayField.Invalidate();
        }

		//
		// Connection button click
		//
		private void ConnectButton_Click(object sender, EventArgs e)
		{
			if (!SerialPort.IsOpen)
			{
                Connect();
			}
			else
			{
                Disconnect(false);
			}
		}

        //
        // Connect port
        //
        private void Connect()
        {
            try
            {
                // Set port params
                SerialPort.PortName = PortSelection.Text;
                SerialPort.BaudRate = Convert.ToInt32(BaudRateSelection.Text);
                SerialPort.DataBits = 8;
                SerialPort.Parity = (Parity)Enum.Parse(typeof(Parity), ParitySelection.SelectedItem.ToString());
                SerialPort.Handshake = Handshake.None;
                SerialPort.StopBits = StopBits.One;
                SerialPort.Open();

                // Flush buffers
                SerialPort.DiscardInBuffer();
                SerialPort.DiscardOutBuffer();
            }
            catch (Exception)
            {
                ConnectionStatus.Text = "Error opening port";                
            }
            finally
            {
                // Last update timestamp
                lastUpdate = DateTime.Now;
                UpdateTimer.Interval = (int)UpdateInterval.Value;

                // Delayed start ?                
                DelayTimer.Enabled = false;
                UpdateTimer.Enabled = true;                

                // Set active remote
                activeRemote = remotes[ControlMethod.SelectedIndex];

                // Prepare remote
                activeRemote.Init();
                activeRemote.sendEvent += SerialDataSend;
                activeRemote.logMessageEvent += LogMessageEventHandler;

                // Show status                
                PortSelection.Enabled = false;
                RefreshPortListButton.Enabled = false;
                BaudRateSelection.Enabled = false;
                ParitySelection.Enabled = false;
                UpdateInterval.Enabled = false;
                ControlMethod.Enabled = false;
                ConnectButton.Text = "&Disconnect";
                ConnectionStatus.Text = "Connected";

                // Set active control
                ActiveControl = PlayField;

                // Working
                working = true;
            }
        }

        //
        // Disconnect port
        //
        private void Disconnect(bool error)
        {
            // Not working anymore
            working = false;
            
            // Stop timers
            DelayTimer.Enabled = false;
            UpdateTimer.Enabled = false;
            
            // Stop remote class
            if (activeRemote != null)
            {
                activeRemote.Close();              
                activeRemote.sendEvent -= SerialDataSend;
                activeRemote.logMessageEvent -= LogMessageEventHandler;
            }

            // Let all delegates finish before closing port
            Application.DoEvents();

            // Close port
            try
            {
                SerialPort.DiscardInBuffer();
                SerialPort.DiscardOutBuffer();                
                SerialPort.Close();               
            }
            catch (Exception)
            {                
            }

            // Show status            
            PortSelection.Enabled = true;
            RefreshPortListButton.Enabled = true;
			BaudRateSelection.Enabled = true;
            ParitySelection.Enabled = true;
            UpdateInterval.Enabled = true;
            ControlMethod.Enabled = true;
			ConnectButton.Text = "&Connect";
			ConnectionStatus.Text = (error ? "Port error, disconnected" : "Not connected");
        }

		//
		// Mouse move
		//
		private void PlayField_MouseMove(object sender, MouseEventArgs e)
		{			
			if (e.Button != MouseButtons.Left) return;
												
			double x = 2 * ((double)e.X / (double)PlayField.Width) - 1;
			double y = 1 - 2 * ((double)e.Y / (double)PlayField.Height);
			
			Drive(x, y, 0, null);	
		}
		
        //
        // Mouse up means no driving
        //
		private void PlayField_MouseUp(object sender, MouseEventArgs e)
		{
            Drive(0, 0, 0, 0);
		}	
		
		//
        // Key down handler
        //
        private void HandleKeyDown(object sender, KeyEventArgs e)
        {            
            if (!downKeys.Contains(e.KeyData))
            {
				downKeys.Add(e.KeyData);
			}
			
            HandleKeys();
            e.SuppressKeyPress = true;
            e.Handled = true;
        }

        //
        // Key up handler
        //
        private void HandleKeyUp(object sender, KeyEventArgs e)
        {
            downKeys.Remove(e.KeyData);
            HandleKeys();
            e.SuppressKeyPress = true;
            e.Handled = true;
        }
		
		//
        // Keys handler
        //
        private void HandleKeys()
        {
            // Calculate x, y, z axis value
            double x = TwoButtonValue(ButtonCheck(Keys.Right), ButtonCheck(Keys.Left));  
            double y = TwoButtonValue(ButtonCheck(Keys.Up), ButtonCheck(Keys.Down));
            double z = TwoButtonValue(ButtonCheck(Keys.X), ButtonCheck(Keys.Z));

            int flags = (ButtonCheck(Keys.Space) ? 0x01 : 0x00) |
                        (ButtonCheck(Keys.F1) ? 0x02 : 0x00) |
                        (ButtonCheck(Keys.F2) ? 0x04 : 0x00) |
                        (ButtonCheck(Keys.F3) ? 0x08 : 0x00) |
                        (ButtonCheck(Keys.F4) ? 0x10 : 0x00) |
                        (ButtonCheck(Keys.F5) ? 0x20 : 0x00) |
                        (ButtonCheck(Keys.F6) ? 0x40 : 0x00) |
                        (ButtonCheck(Keys.F7) ? 0x80 : 0x00);

			// Drive
            Drive(x, y, z, flags);
        }
        
        //
        // Value from two keys
        //
        private double TwoButtonValue(bool up, bool down)
        {
            return (up ? 1 : 0) - (down ? 1 : 0);
        }      

        //
        // Check if button pressed
        //
        private bool ButtonCheck(Keys key)
        {                            
            return downKeys.Contains(key);            
        }
		
		//
		// Drive command
		//
        private void Drive(double? x, double? y, double? z, int? flags)
		{
			double maxSpeed = (double)TopSpeed.Value / (double)100;

            if (x.HasValue)
            {
                destinationSpeed.x = Limit(x.Value, -maxSpeed, maxSpeed);
            }

            if (y.HasValue)
            {
                destinationSpeed.y = Limit(y.Value, -maxSpeed, maxSpeed);
            }

            if (z.HasValue)
            {
                destinationSpeed.z = Limit(z.Value, -maxSpeed, maxSpeed);
            }

            if (flags.HasValue)
            {
                values.Flags = flags.Value;
            }
		}
		
		//
		// Actual driving
        // Call current RC method        
		//
		private void DoDriving()
		{
			double acceleration = (double)Acceleration.Value / (double)100;
			DateTime currentUpdate = DateTime.Now;
			TimeSpan deltaTime;            
			
			// Exponential acceleration
            deltaTime     = new TimeSpan(currentUpdate.Ticks - lastUpdate.Ticks);
			acceleration  = Math.Pow(acceleration, 2);
			acceleration *= (double)deltaTime.Milliseconds / (double)UpdateTimer.Interval;
			lastUpdate    = currentUpdate;
												
            // Accelerate to get destination value
            Accelerate(ref currentSpeed.x, destinationSpeed.x, acceleration);
			Accelerate(ref currentSpeed.y, destinationSpeed.y, acceleration);
            Accelerate(ref currentSpeed.z, destinationSpeed.z, acceleration);
            
            // Make control values            
            values.Acceleration = acceleration;
            values.X = (CheckInvertX.Checked ? -currentSpeed.x : currentSpeed.x);
            values.Y = (CheckInvertY.Checked ? -currentSpeed.y : currentSpeed.y);
            values.Z = (CheckInvertZ.Checked ? -currentSpeed.z : currentSpeed.z);            

            // Call control function
            remotes[ControlMethod.SelectedIndex].Control(values);
			
			// Redraw playfield			
			PlayField.Invalidate();
		}
	       
        //
        // Accleration function
        //
		private void Accelerate(ref double current, double destination, double accel)
		{
			if (Math.Abs(current - destination) <= accel)
			{
				current = destination;
			}
			else if (current < destination)
			{
				current += accel;
			}
			else if (current > destination)
			{
				current -= accel;
			}
		}
		
		//
		// Paint event
		//
		private void PlayField_Paint(object sender, PaintEventArgs e)
		{
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            // Create quadrant (1/4th of area)
            Point center = new Point(PlayField.Width / 2, PlayField.Height / 2);
            Point quadrant = new Point(center.X, center.Y);

            // Set center point
            e.Graphics.TranslateTransform(center.X, center.Y);

            // Clear area
            e.Graphics.Clear(SystemColors.Control);            

            // Call RC specific paint function
            remotes[ControlMethod.SelectedIndex].Paint(e.Graphics, quadrant);
        }

		//
		// Timer tick
		//
		private void UpdateTimer_Tick(object sender, EventArgs e)
		{
            // Check if port is still open
            if (SerialPort.IsOpen)
            {
                bool canDrive = false;

                try
                {
                    canDrive = (SerialPort.BytesToWrite == 0);
                }
                catch (Exception)
                {
                    Disconnect(true);                    
                }
                finally
                {
                    if (canDrive)
                    {
                        DoDriving();
                    }
                }
            }
		}

		//
		// Delay timer tick
		//
		private void DelayTimer_Tick(object sender, EventArgs e)
		{			
			DelayTimer.Enabled  = false;
			UpdateTimer.Enabled = true;
		}

        //
        // Port list update button
        //
        private void RefreshPortListButton_Click(object sender, EventArgs e)
        {
            RefreshPortList();
        }

        //
        // Buffer hex value
        //
        private string BufferHexValue(byte[] buffer)
        {
            string str = "";

            foreach (byte b in buffer)
            {
                if (str != "") str += " ";
                str += b.ToString("X02");
            }

            return str;
        }

        //
        // Double value limiting function
        //
        public double Limit(double v, double low, double high)
        {
            return Math.Min(high, Math.Max(low, v));
        }

        //
        // Log message event handler
        //
        private void LogMessageEventHandler(string message)
        {
            if (working)
            {
                Invoke(new DisplayMessageDelegate(DisplayMessage), new object[] { message });
            }
        }

        //
        // Serial data receiving method
        //        
        private void SerialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Read all bytes that can be read
            try
            {
                int size = SerialPort.BytesToRead;
                byte[] buffer = new byte[size];
                SerialPort.Read(buffer, 0, size);
                activeRemote.DataReceived(buffer);
            }
            catch (Exception)
            {                
                //Disconnect(true);
            }
        }

        //
        // Send buffer
        //
        private void SerialDataSend(byte[] buffer, int count)
        {
            try
            {
                if (SerialPort.IsOpen)
                {
                    SerialPort.Write(buffer, 0, count);
                }
            }
            catch (Exception)
            {
                Disconnect(true);
            }
        }

        //
        // Display message
        //
        private void DisplayMessage(string message)
        {
            if (working && listBox.Visible)
            {
                listBox.BeginUpdate();
                while (listBox.Items.Count > 10000)
                {
                    listBox.Items.RemoveAt(0);
                }                
                listBox.Items.Add(message);
                listBox.SelectedIndex = listBox.Items.Count - 1;
                listBox.EndUpdate();
            }
        }

        private void ExpandLogButton_Click(object sender, EventArgs e)
        {
            LogVisible = !LogVisible;
        }

        /// <summary>
        /// Log visible property
        /// </summary>
        private bool LogVisible
        {
            get => listBox.Visible;
            set
            {
                if (value)
                {
                    listBox.Visible = true;
                    ExpandLogButton.Image = global::RobotRemoteControl.Properties.Resources.DoubleLeftArrowHS;
                }
                else
                {
                    listBox.Visible = false;
                    ExpandLogButton.Image = global::RobotRemoteControl.Properties.Resources.DoubleRightArrowHS;
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Disconnect before closing
            if (working)
            {
                Disconnect(false);
            }
        }

        private void PlayField_SizeChanged(object sender, EventArgs e)
        {
            PlayField.Refresh();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveSettings();
        }
	}
}
