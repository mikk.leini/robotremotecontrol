﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Drawing;
using System.Windows.Forms;

namespace RobotRemoteControl
{
    public class RCOneByteThreeAxis : RC
    {
        protected double dDrive, dStrafe, dTurn;

        //
        // Name
        //
        override public string Name
        {
            get { return "One byte three axis drive"; }
        }

        //
        // Control
        //
        override public void Control(ControlValues values)
        {
            byte[] buffer = new byte[4];
            sbyte sbDrive, sbStrafe, sbTurn;

            // Convert from X/Y/Z to drive/strafe/turn
            dDrive = Limit(values.Y, -1, 1);
            dStrafe = Limit(values.X, -1, 1);
            dTurn = Limit(values.Z, -1, 1);

            // Convert to 7 bit binary
            sbDrive = (sbyte)(dDrive * 31);
            sbStrafe = (sbyte)(dStrafe * 31);
            sbTurn = (sbyte)(dTurn * 31);

            // Form packet
            buffer[0] = (byte)(0x00 | (((byte)sbDrive) & 0x3F));
            buffer[1] = (byte)(0x40 | (((byte)sbStrafe) & 0x3F));
            buffer[2] = (byte)(0x80 | (((byte)sbTurn) & 0x3F));
            buffer[3] = (byte)(0xC0 | (((byte)values.Flags & 0x3F)));

            // Send
            Send(buffer);
        }

        //
        // Paint indicators
        //
        override public void Paint(Graphics gfx, Point quad)
        {
            int sf = -(int)(dDrive * quad.Y);
            int ss =  (int)(dStrafe * quad.X);
            int st =  (int)(dTurn * quad.X);

            Color colF = CreateSpeedColor(dDrive);
            Color colS = CreateSpeedColor(dStrafe, true);
            Color colT = CreateSpeedColor(dTurn);

            gfx.DrawLine(new Pen(colF, 11), new Point(-quad.X + 6, 0), new Point(-quad.X + 6, sf));
            gfx.DrawLine(new Pen(colF, 11), new Point(quad.X - 6, 0), new Point(quad.X - 6, sf));
            gfx.DrawLine(new Pen(colS, 11), new Point(0, -quad.Y + 6), new Point(ss, -quad.Y + 6));
            gfx.DrawLine(new Pen(colT, 11), new Point(0, quad.Y - 6), new Point(st, quad.Y - 6));

            DrawCross(gfx, quad);
        }

        //
        // Data reception event
        //
        override public void DataReceived(byte[] buffer)
        {
            // Nothing to receive
        }
    }
}
