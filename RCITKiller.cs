﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Drawing;
using System.Windows.Forms;

namespace RobotRemoteControl
{
    class RCITKiller : RC
    {
        private const double maxRPM = 8000;

        enum ReceiveState
        {
            WaitPreambula,
            WaitID,
            WaitLength,
            WaitData,
            WaitChecksum1,
            WaitChecksum2
        };

        private ReceiveState receiveState = ReceiveState.WaitPreambula;
        private int receiveID, receiveLength, receiveIndex;
        private ushort receiveChecksum;
        private byte[] receiveData = new byte[256];
        private ControlValues lastValues = new ControlValues();
        private double[] motorControlSpeed = new double[3];
        private double[] motorStatusSpeed = new double[3];
        private short batteryVoltage = 0;
        private bool kick = false, dribblerOn = false;

        //
        // Name
        //
        override public string Name
        {
            get { return "IT-killer specific drive"; }
        }

        //
        // Control
        //
        override public void Control(ControlValues values)
        {         
            // Remember last values
            lastValues = values;

            // Convert from X/Y/Z to differential
            motorControlSpeed[0] = -values.Y + values.X + values.Z;
            motorControlSpeed[1] =  values.Y + values.X + values.Z;
            motorControlSpeed[2] = -2.0 * values.X + values.Z;

            // Limit values
            motorControlSpeed[0] = Limit(motorControlSpeed[0], -1, 1);
            motorControlSpeed[1] = Limit(motorControlSpeed[1], -1, 1);
            motorControlSpeed[2] = Limit(motorControlSpeed[2], -1, 1);

            // Send speed control command
            SendSpeedControl(
                (short)(motorControlSpeed[0] * maxRPM),
                (short)(motorControlSpeed[1] * maxRPM),
                (short)(motorControlSpeed[2] * maxRPM));

            // Space press = ball kick ?
            kick = ((values.Flags & 0x01) > 0);            

            // Dribbler on or off ?
            if ((values.Flags & 0x02) > 0)
            {
                dribblerOn = true;
            }
            else if ((values.Flags & 0x04) > 0)
            {
                dribblerOn = false;
            }

            // Send ball control
            SendBallControl(true, kick, 0, (uint)(dribblerOn ? 20 : 0));
        }

        //
        // Paint indicators
        //
        override public void Paint(Graphics gfx, Point quad)
        {
            float radius = Math.Min(quad.X, quad.Y) * 0.8f;

            DrawCross(gfx, quad);

            gfx.DrawEllipse(Pens.Black, -radius, -radius, 2 * radius, 2 * radius);
            
            DrawRotation(gfx, radius);

            // Draw control bars
            DrawSpeedStatus(gfx, radius - 2, +60, (float)motorControlSpeed[0]);
            DrawSpeedStatus(gfx, radius - 2, -60, (float)motorControlSpeed[1]);
            DrawSpeedStatus(gfx, radius - 2, 180, (float)motorControlSpeed[2]);

            // Draw status bars
            DrawSpeedStatus(gfx, radius + 2, +60, (float)motorStatusSpeed[0]);
            DrawSpeedStatus(gfx, radius + 2, -60, (float)motorStatusSpeed[1]);
            DrawSpeedStatus(gfx, radius + 2, 180, (float)motorStatusSpeed[2]);

            // Display speed values
            DrawMotorSpeed(gfx, new PointF( radius, -radius), (float)motorStatusSpeed[0]);
            DrawMotorSpeed(gfx, new PointF(-radius, -radius), (float)motorStatusSpeed[1]);
            DrawMotorSpeed(gfx, new PointF(0, radius + 13),   (float)motorStatusSpeed[2]);

            // Draw battery voltage
            DrawBatteryVoltage(gfx, new PointF(-quad.X + 5, -quad.Y + 5), batteryVoltage);
        }

        //
        // Draw rotation arc
        //
        private void DrawRotation(Graphics gfx, float radius)
        {
            Pen arcPen = new Pen(Color.Green, 3);

            // Draw arc
            gfx.DrawArc(arcPen,
                -25, -25,
                50, 50,
                270, (float)(lastValues.Z * 90));
        }

        //
        // Draw speed status
        //
        private void DrawSpeedStatus(Graphics gfx, float radius, float angle, float speed)
        {
            // Decide color
            int cs = 127 + (int)(speed * 255);
            cs = Math.Min(255, Math.Max(0, cs));
            Color arcColor = Color.FromArgb(255, 255 - cs, 0, cs);
            
            // Set pen
            Pen arcPen = new Pen(arcColor, 3);
            
            // Draw arc
            gfx.DrawArc(arcPen,
                -radius, -radius,
                radius * 2, radius * 2,
                angle - 90, speed * 30);
        }

        //
        // Draw motor speed value in numbers
        //
        private void DrawMotorSpeed(Graphics gfx, PointF mid, float speed)
        {
            int speedInt = (int)(speed * maxRPM);
            string speedStr = speedInt.ToString();
            Font font = new Font(FontFamily.GenericSansSerif, 9);
            SizeF strSize = gfx.MeasureString(speedStr, font);

            gfx.DrawString(speedStr, font, Brushes.Black, mid.X - strSize.Width / 2, mid.Y - strSize.Height / 2);
        }

        //
        // Draw motor speed value in numbers
        //
        private void DrawBatteryVoltage(Graphics gfx, PointF mid, short millivolts)
        {
            double volts = millivolts / 1000;
            string voltStr = String.Format("{0:0.0}", volts) + "V";
            Font font = new Font(FontFamily.GenericSansSerif, 9);
            gfx.DrawString(voltStr, font, Brushes.Black, mid);
        }

        //
        // Speed sending sending
        //        
        private void SendSpeedControl(short motor1, short motor2, short motor3)
        {
            byte[] data = new byte[6];

            data[0] = (byte)(((ushort)motor1 >> 8) & 0xFF);
            data[1] = (byte)((ushort)motor1 & 0xFF);
            data[2] = (byte)(((ushort)motor2 >> 8) & 0xFF);
            data[3] = (byte)((ushort)motor2 & 0xFF);
            data[4] = (byte)(((ushort)motor3 >> 8) & 0xFF);
            data[5] = (byte)((ushort)motor3 & 0xFF);

            SendMessage(0xD0, 6, data);
        }

        //
        // Ball control sending
        //
        private void SendBallControl(bool charge, bool kick, uint kickTime, uint dribbler)
        {
            ulong value;
            byte[] data = new byte[4];

            // Put values into 32 bit variable and then convert it to byte array
            // Bit address are from highest to lowest.
            value =
                (ulong)(charge ? (1 << 7) : 0x00) |
                (ulong)(dribbler & 0x07F) |
                (ulong)((kickTime & 0xFF) << 8) |
                (ulong)(((kickTime >> 8) & 0x3) << 22) |
                (ulong)(kick ? (1 << 21) : 0x00);
                        
            data = BitConverter.GetBytes(value);

            SendMessage(0xB0, 3, data);
            //LogMessage("B0 = " + BinaryToHexString(data, 3));
        }

        //
        // Message sending
        //
        protected void SendMessage(byte id, byte length, byte[] data)
        {
            byte[] buffer = new byte[length + 5];
            ushort checksum = 0;
            int i;

            // Form packet
            buffer[0] = 0xAA;
            buffer[1] = id;
            buffer[2] = length;

            for (i = 0; i < 3; i++)
            {
                checksum += buffer[i];
            }

            for (i = 0; i < length; i++)
            {
                buffer[3 + i] = data[i];
                checksum += data[i];
            }

            // Checksum
            buffer[3 + length] = (byte)(checksum >> 8);
            buffer[4 + length] = (byte)(checksum & 0xFF);

            // Write to send buffer
            Send(buffer, 5 + length);
            
            //LogMessage("Send " + BinaryToHexString(buffer, 5 + length));
        }

        //
        // Data reception event
        //
        override public void DataReceived(byte[] buffer)
        {
            // Parse all bytes
            foreach (byte b in buffer)
            {
                switch (receiveState)
                {
                    case ReceiveState.WaitPreambula:
                        if (b == 0xAA)
                        {
                            receiveState = ReceiveState.WaitID;
                            receiveIndex = 0;
                            receiveChecksum = b;
                        }
                        else
                        {
                            // Unexpected byte...
                        }
                        break;

                    case ReceiveState.WaitID:
                        receiveID = b;
                        receiveChecksum += b;
                        receiveState = ReceiveState.WaitLength;
                        break;

                    case ReceiveState.WaitLength:
                        receiveLength = b;
                        receiveChecksum += b;
                        receiveState = ReceiveState.WaitData;
                        break;

                    case ReceiveState.WaitData:
                        receiveData[receiveIndex++] = b;
                        receiveChecksum += b;

                        // Got last byte ?
                        if (receiveIndex >= receiveLength)
                        {
                            receiveState = ReceiveState.WaitChecksum1;
                        }
                        break;

                    case ReceiveState.WaitChecksum1:                        
                        // Check checksum high byte
                        if (b == (receiveChecksum >> 8))
                        {                            
                            receiveState = ReceiveState.WaitChecksum2;
                        }
                        else
                        {
                            receiveState = ReceiveState.WaitPreambula;
                        }
                        break;

                    case ReceiveState.WaitChecksum2:                        
                        // Check checksum low byte
                        if (b == (receiveChecksum & 0xFF))
                        {
                            // Correct message
                            HandleMessage(receiveID, receiveData, receiveLength);
                        }
                        receiveState = ReceiveState.WaitPreambula;
                        break;
                }
            }
        }

        //
        // Message handler
        //
        protected void HandleMessage(int id, byte[] buffer, int length)
        {
            LogMessage(id.ToString() + " = " + BitConverter.ToString(buffer, 0, length));

            switch (id)
            {
                case 0xD1:
                case 0xD2:
                case 0xD3:
                    if (length == 5)
                    {
                        motorStatusSpeed[id - 0xD1] = (double)(((short)BitConverter.ToInt16(buffer, 2)) / maxRPM);
                    }
                    break;

                case 0xA0:
                    if (length == 2)
                    {
                        batteryVoltage = (short)BitConverter.ToInt16(buffer, 2);
                    }
                    break;
            }
        }
    }
}
