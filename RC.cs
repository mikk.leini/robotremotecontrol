﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace RobotRemoteControl
{
    public delegate void SendEventHandler(byte[] buffer, int count);
    public delegate void LogMessageEventHandler(string message);
           
    abstract public class RC
    {        
        public SendEventHandler sendEvent;
        public LogMessageEventHandler logMessageEvent;

        //
        // Initialization
        //
        public virtual void Init()
        {
        }

        //
        // Closing
        //
        public virtual void Close()
        {
        }

        //
		// Double value limiting function
		//
        public static double Limit(double v, double low, double high)
        {
            return Math.Min(high, Math.Max(low, v));
        }

        //
        // Message logging
        //
        protected void LogMessage(string message)
        {
            logMessageEvent?.Invoke(DateTime.Now.ToLongTimeString() + " " + message);
        }

        //
        // Send buffer
        //
        protected void Send(byte[] buffer)
        {
            sendEvent?.Invoke(buffer, buffer.Length);
        }

        //
        // Send buffer
        //
        protected void Send(byte[] buffer, int count)
        {
            sendEvent?.Invoke(buffer, count);
        }

        //
        // Hex to string
        //
        protected string BinaryToHexString(byte[] buffer, int length)
        {
            string s = "";

            for (int i = 0; i < length; i++)
            {
                s += buffer[i].ToString("X02") + " ";
            }

            return s;
        }

        /// <summary>
        /// Draw cross
        /// </summary>
        /// <param name="gfx"></param>
        /// <param name="quad"></param>
        protected void DrawCross(Graphics gfx, Point quad)
        {
            // Draw cross
            gfx.DrawLine(new Pen(Color.Gray, 1), new Point(-quad.X, 0), new Point(quad.X, 0));
            gfx.DrawLine(new Pen(Color.Gray, 1), new Point(0, -quad.Y), new Point(0, quad.Y));
        }

        /// <summary>
        /// Draw pointer
        /// </summary>
        /// <param name="gfx"></param>
        /// <param name="pointer"></param>
        protected void DrawPointer(Graphics gfx, Point pointer)
        {
            gfx.DrawLine(new Pen(Color.Green, 3), new Point(0, 0), pointer);
            gfx.FillEllipse(new SolidBrush(Color.Green), pointer.X - 5, pointer.Y - 5, 10, 10);            
        }

        /// <summary>
        /// Create speed color
        /// </summary>
        /// <param name="speed"></param>
        /// <returns></returns>
        protected Color CreateSpeedColor(double speed, bool useGreen = false)
        {
            speed = Limit(speed, -1, 1);
            int speedI = (int)(speed * 127.0);

            if (!useGreen)
            {
                return Color.FromArgb(255, 127 - speedI, 0, 127 + speedI);
            }
            else
            {
                return Color.FromArgb(255, 127 - speedI, 127 + Math.Abs(speedI), 0);
            }
        }

        /// <summary>
        /// Abstract name getting function
        /// </summary>
        abstract public string Name { get; }

        /// <summary>
        /// Abstract work function
        /// </summary>
        /// <param name="values"></param>
        abstract public void Control(ControlValues values);

        /// <summary>
        /// Abstract paint function
        /// </summary>
        /// <param name="gfx"></param>
        /// <param name="quad"></param>
        abstract public void Paint(Graphics gfx, Point quad);

        /// <summary>
        /// Abstract data reception handler
        /// </summary>
        /// <param name="buffer"></param>
        abstract public void DataReceived(byte[] buffer);   
    }
}
