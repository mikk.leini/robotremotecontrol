﻿namespace RobotRemoteControl
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.SerialPort = new System.IO.Ports.SerialPort(this.components);
            this.UpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.ConnectionStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.PortOutputData = new System.Windows.Forms.ToolStripStatusLabel();
            this.DelayTimer = new System.Windows.Forms.Timer(this.components);
            this.listBox = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.PlayField = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.CheckInvertZ = new System.Windows.Forms.CheckBox();
            this.CheckInvertY = new System.Windows.Forms.CheckBox();
            this.CheckInvertX = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Acceleration = new System.Windows.Forms.TrackBar();
            this.TopSpeed = new System.Windows.Forms.TrackBar();
            this.KeyBox = new System.Windows.Forms.TextBox();
            this.Parity = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ParitySelection = new System.Windows.Forms.ComboBox();
            this.ExpandLogButton = new System.Windows.Forms.Button();
            this.ControlMethod = new System.Windows.Forms.ComboBox();
            this.RefreshPortListButton = new System.Windows.Forms.Button();
            this.PortSelection = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.UpdateInterval = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.BaudRateSelection = new System.Windows.Forms.ComboBox();
            this.ConnectButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlayField)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Acceleration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopSpeed)).BeginInit();
            this.Parity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // SerialPort
            // 
            this.SerialPort.BaudRate = 38400;
            // 
            // UpdateTimer
            // 
            this.UpdateTimer.Interval = 25;
            this.UpdateTimer.Tick += new System.EventHandler(this.UpdateTimer_Tick);
            // 
            // StatusBar
            // 
            this.StatusBar.Location = new System.Drawing.Point(0, 519);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(644, 22);
            this.StatusBar.SizingGrip = false;
            this.StatusBar.TabIndex = 9;
            // 
            // ConnectionStatus
            // 
            this.ConnectionStatus.Name = "ConnectionStatus";
            this.ConnectionStatus.Size = new System.Drawing.Size(86, 17);
            this.ConnectionStatus.Text = "Not connected";
            this.ConnectionStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PortOutputData
            // 
            this.PortOutputData.Name = "PortOutputData";
            this.PortOutputData.Size = new System.Drawing.Size(0, 17);
            // 
            // DelayTimer
            // 
            this.DelayTimer.Tick += new System.EventHandler(this.DelayTimer_Tick);
            // 
            // listBox
            // 
            this.listBox.Dock = System.Windows.Forms.DockStyle.Right;
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(444, 0);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(200, 519);
            this.listBox.TabIndex = 61;
            this.listBox.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.Parity);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(444, 519);
            this.panel1.TabIndex = 62;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.PlayField);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 146);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(5, 2, 5, 2);
            this.panel2.Size = new System.Drawing.Size(444, 288);
            this.panel2.TabIndex = 46;
            // 
            // PlayField
            // 
            this.PlayField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PlayField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PlayField.Location = new System.Drawing.Point(5, 2);
            this.PlayField.Name = "PlayField";
            this.PlayField.Size = new System.Drawing.Size(434, 284);
            this.PlayField.TabIndex = 46;
            this.PlayField.TabStop = false;
            this.PlayField.SizeChanged += new System.EventHandler(this.PlayField_SizeChanged);
            this.PlayField.Paint += new System.Windows.Forms.PaintEventHandler(this.PlayField_Paint);
            this.PlayField.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PlayField_MouseMove);
            this.PlayField.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PlayField_MouseUp);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.CheckInvertZ);
            this.panel3.Controls.Add(this.CheckInvertY);
            this.panel3.Controls.Add(this.CheckInvertX);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.Acceleration);
            this.panel3.Controls.Add(this.TopSpeed);
            this.panel3.Controls.Add(this.KeyBox);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 434);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(444, 85);
            this.panel3.TabIndex = 1;
            // 
            // CheckInvertZ
            // 
            this.CheckInvertZ.AutoSize = true;
            this.CheckInvertZ.Location = new System.Drawing.Point(154, 35);
            this.CheckInvertZ.Name = "CheckInvertZ";
            this.CheckInvertZ.Size = new System.Drawing.Size(63, 17);
            this.CheckInvertZ.TabIndex = 12;
            this.CheckInvertZ.Text = "Invert Z";
            this.CheckInvertZ.UseVisualStyleBackColor = true;
            // 
            // CheckInvertY
            // 
            this.CheckInvertY.AutoSize = true;
            this.CheckInvertY.Location = new System.Drawing.Point(85, 35);
            this.CheckInvertY.Name = "CheckInvertY";
            this.CheckInvertY.Size = new System.Drawing.Size(63, 17);
            this.CheckInvertY.TabIndex = 11;
            this.CheckInvertY.Text = "Invert Y";
            this.CheckInvertY.UseVisualStyleBackColor = true;
            // 
            // CheckInvertX
            // 
            this.CheckInvertX.AutoSize = true;
            this.CheckInvertX.Location = new System.Drawing.Point(12, 35);
            this.CheckInvertX.Name = "CheckInvertX";
            this.CheckInvertX.Size = new System.Drawing.Size(63, 17);
            this.CheckInvertX.TabIndex = 10;
            this.CheckInvertX.Text = "Invert X";
            this.CheckInvertX.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(171, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 64;
            this.label3.Text = "Acceleration:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 63;
            this.label2.Text = "Top speed:";
            // 
            // Acceleration
            // 
            this.Acceleration.AutoSize = false;
            this.Acceleration.Location = new System.Drawing.Point(240, 7);
            this.Acceleration.Maximum = 100;
            this.Acceleration.Minimum = 1;
            this.Acceleration.Name = "Acceleration";
            this.Acceleration.Size = new System.Drawing.Size(92, 24);
            this.Acceleration.TabIndex = 9;
            this.Acceleration.TickFrequency = 25;
            this.Acceleration.TickStyle = System.Windows.Forms.TickStyle.None;
            this.Acceleration.Value = 40;
            // 
            // TopSpeed
            // 
            this.TopSpeed.AutoSize = false;
            this.TopSpeed.Location = new System.Drawing.Point(74, 7);
            this.TopSpeed.Maximum = 100;
            this.TopSpeed.Name = "TopSpeed";
            this.TopSpeed.Size = new System.Drawing.Size(86, 30);
            this.TopSpeed.TabIndex = 8;
            this.TopSpeed.TickFrequency = 25;
            this.TopSpeed.TickStyle = System.Windows.Forms.TickStyle.None;
            this.TopSpeed.Value = 100;
            // 
            // KeyBox
            // 
            this.KeyBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.KeyBox.Location = new System.Drawing.Point(10, 58);
            this.KeyBox.Name = "KeyBox";
            this.KeyBox.Size = new System.Drawing.Size(426, 20);
            this.KeyBox.TabIndex = 13;
            this.KeyBox.Text = "Press keys here...";
            this.KeyBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HandleKeyDown);
            this.KeyBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HandleKeyUp);
            // 
            // Parity
            // 
            this.Parity.Controls.Add(this.label7);
            this.Parity.Controls.Add(this.label4);
            this.Parity.Controls.Add(this.ParitySelection);
            this.Parity.Controls.Add(this.ExpandLogButton);
            this.Parity.Controls.Add(this.ControlMethod);
            this.Parity.Controls.Add(this.RefreshPortListButton);
            this.Parity.Controls.Add(this.PortSelection);
            this.Parity.Controls.Add(this.label6);
            this.Parity.Controls.Add(this.UpdateInterval);
            this.Parity.Controls.Add(this.label5);
            this.Parity.Controls.Add(this.BaudRateSelection);
            this.Parity.Controls.Add(this.ConnectButton);
            this.Parity.Controls.Add(this.label1);
            this.Parity.Dock = System.Windows.Forms.DockStyle.Top;
            this.Parity.Location = new System.Drawing.Point(0, 0);
            this.Parity.Name = "Parity";
            this.Parity.Size = new System.Drawing.Size(444, 146);
            this.Parity.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 73;
            this.label7.Text = "Robot:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 72;
            this.label4.Text = "Parity:";
            // 
            // ParitySelection
            // 
            this.ParitySelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ParitySelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ParitySelection.FormattingEnabled = true;
            this.ParitySelection.Location = new System.Drawing.Point(70, 93);
            this.ParitySelection.Name = "ParitySelection";
            this.ParitySelection.Size = new System.Drawing.Size(249, 21);
            this.ParitySelection.TabIndex = 3;
            // 
            // ExpandLogButton
            // 
            this.ExpandLogButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExpandLogButton.Image = global::RobotRemoteControl.Properties.Resources.DoubleRightArrowHS;
            this.ExpandLogButton.Location = new System.Drawing.Point(404, 38);
            this.ExpandLogButton.Name = "ExpandLogButton";
            this.ExpandLogButton.Size = new System.Drawing.Size(31, 103);
            this.ExpandLogButton.TabIndex = 7;
            this.ExpandLogButton.UseVisualStyleBackColor = true;
            this.ExpandLogButton.Click += new System.EventHandler(this.ExpandLogButton_Click);
            // 
            // ControlMethod
            // 
            this.ControlMethod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ControlMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ControlMethod.FormattingEnabled = true;
            this.ControlMethod.Location = new System.Drawing.Point(70, 12);
            this.ControlMethod.Name = "ControlMethod";
            this.ControlMethod.Size = new System.Drawing.Size(365, 21);
            this.ControlMethod.TabIndex = 0;
            this.ControlMethod.SelectedIndexChanged += new System.EventHandler(this.ControlMethod_SelectedIndexChanged);
            // 
            // RefreshPortListButton
            // 
            this.RefreshPortListButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RefreshPortListButton.Image = ((System.Drawing.Image)(resources.GetObject("RefreshPortListButton.Image")));
            this.RefreshPortListButton.Location = new System.Drawing.Point(326, 38);
            this.RefreshPortListButton.Name = "RefreshPortListButton";
            this.RefreshPortListButton.Size = new System.Drawing.Size(72, 22);
            this.RefreshPortListButton.TabIndex = 5;
            this.RefreshPortListButton.UseVisualStyleBackColor = true;
            this.RefreshPortListButton.Click += new System.EventHandler(this.RefreshPortListButton_Click);
            // 
            // PortSelection
            // 
            this.PortSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PortSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PortSelection.FormattingEnabled = true;
            this.PortSelection.Location = new System.Drawing.Point(70, 39);
            this.PortSelection.Name = "PortSelection";
            this.PortSelection.Size = new System.Drawing.Size(250, 21);
            this.PortSelection.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 66;
            this.label6.Text = "Baudrate:";
            // 
            // UpdateInterval
            // 
            this.UpdateInterval.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UpdateInterval.Location = new System.Drawing.Point(70, 120);
            this.UpdateInterval.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.UpdateInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.UpdateInterval.Name = "UpdateInterval";
            this.UpdateInterval.Size = new System.Drawing.Size(249, 20);
            this.UpdateInterval.TabIndex = 4;
            this.UpdateInterval.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 65;
            this.label5.Text = "Interval:";
            // 
            // BaudRateSelection
            // 
            this.BaudRateSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BaudRateSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BaudRateSelection.FormattingEnabled = true;
            this.BaudRateSelection.Items.AddRange(new object[] {
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200",
            "230400"});
            this.BaudRateSelection.Location = new System.Drawing.Point(70, 66);
            this.BaudRateSelection.Name = "BaudRateSelection";
            this.BaudRateSelection.Size = new System.Drawing.Size(249, 21);
            this.BaudRateSelection.TabIndex = 2;
            // 
            // ConnectButton
            // 
            this.ConnectButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ConnectButton.Image = global::RobotRemoteControl.Properties.Resources.lightningBolt_16xLG;
            this.ConnectButton.Location = new System.Drawing.Point(325, 66);
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(73, 75);
            this.ConnectButton.TabIndex = 6;
            this.ConnectButton.Text = "Connect";
            this.ConnectButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ConnectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ConnectButton.UseVisualStyleBackColor = true;
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 61;
            this.label1.Text = "Port:";
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(441, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 519);
            this.splitter1.TabIndex = 63;
            this.splitter1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 541);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.listBox);
            this.Controls.Add(this.StatusBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(350, 580);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Robot remote control";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HandleKeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HandleKeyUp);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PlayField)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Acceleration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopSpeed)).EndInit();
            this.Parity.ResumeLayout(false);
            this.Parity.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateInterval)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private System.IO.Ports.SerialPort SerialPort;
		private System.Windows.Forms.Timer UpdateTimer;
		private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.ToolStripStatusLabel ConnectionStatus;
        private System.Windows.Forms.Timer DelayTimer;
        private System.Windows.Forms.ToolStripStatusLabel PortOutputData;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox CheckInvertZ;
        private System.Windows.Forms.CheckBox CheckInvertY;
        private System.Windows.Forms.CheckBox CheckInvertX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar Acceleration;
        private System.Windows.Forms.TrackBar TopSpeed;
        private System.Windows.Forms.TextBox KeyBox;
        private System.Windows.Forms.Panel Parity;
        private System.Windows.Forms.Button ExpandLogButton;
        private System.Windows.Forms.ComboBox ControlMethod;
        private System.Windows.Forms.Button RefreshPortListButton;
        private System.Windows.Forms.ComboBox PortSelection;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown UpdateInterval;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox BaudRateSelection;
        private System.Windows.Forms.Button ConnectButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ParitySelection;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox PlayField;
    }
}