﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Drawing;
using System.Windows.Forms;

namespace RobotRemoteControl
{
    public class RCOneByteDifferential : RC
    {
        private double dLeft, dRight;

        //
        // Name
        //
        override public string Name
        {
            get { return "One byte differential drive"; }
        }

        //
        // Control
        //
        override public void Control(ControlValues values)
        {
            byte[] buffer = new byte[2];            
            sbyte sbLeft, sbRight;

            // Convert from X/Y to differential
            dLeft  = Limit(values.Y + values.X, -1.0f, 1.0f);
            dRight = Limit(values.Y - values.X, -1.0f, 1.0f);

            // Convert to 7 bit binary
            sbLeft  = (sbyte)(dLeft * 63);
            sbRight = (sbyte)(dRight * 63);

            // Form packet
            buffer[0] = (byte)sbLeft;
            buffer[1] = (byte)sbRight;

            // Send command
            Send(buffer);
        }

        //
        // Paint indicators
        //
        override public void Paint(Graphics gfx, Point quad)
        {
            int sl = (int)(dLeft  * quad.Y);
            int sr = (int)(dRight * quad.Y);

            gfx.DrawLine(new Pen(CreateSpeedColor(dLeft), 11), new Point(-quad.X + 6, 0), new Point(-quad.X + 6, -sl));
            gfx.DrawLine(new Pen(CreateSpeedColor(dRight), 11), new Point(quad.X - 6, 0), new Point(quad.X - 6, -sr));

            DrawCross(gfx, quad);
        }

        //
        // Data reception event
        //
        override public void DataReceived(byte[] buffer)
        {
            // Nothing to receive
        }
    }
}
