﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Drawing;
using System.Windows.Forms;

namespace RobotRemoteControl
{
    public class RCPacketThreeAxis : RCOneByteThreeAxis
    {        
        //
        // Name
        //
        override public string Name
        {
            get { return "Packet three axis drive"; }
        }

        //
        // Control
        //
        override public void Control(ControlValues values)
        {
            byte[] buffer = new byte[4];
            sbyte sbPre, sbDrive, sbStrafe, sbTurn;

            // Convert from X/Y/Z to drive/strafe/turn
            dDrive = Limit(values.Y, -1.0, 1.0);
            dStrafe = Limit(values.X, -1.0, 1.0);
            dTurn = Limit(values.Z, -1.0, 1.0);

            // Convert to 8-bit signed
            sbPre = (sbyte)-128;
            sbDrive = (sbyte)(dDrive * 127);
            sbStrafe = (sbyte)(dStrafe * 127);
            sbTurn = (sbyte)(dTurn * 127);

            // Form packet
            buffer[0] = (byte)sbPre;
            buffer[1] = (byte)sbStrafe;
            buffer[2] = (byte)sbDrive;
            buffer[3] = (byte)sbTurn;

            // Send
            Send(buffer);
        }
    }
}
