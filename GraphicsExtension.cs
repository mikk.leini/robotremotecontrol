﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotRemoteControl
{
    public static class GraphicsExtension
    {
        /// <summary>
        /// Draw centered string
        /// </summary>
        /// <param name="gfx"></param>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="brush"></param>
        /// <param name="midPos"></param>
        public static void DrawCenterString(this Graphics gfx, string text, Font font, Brush brush, Point midPos, Color? backColor = null)
        {
            SizeF textSize = gfx.MeasureString(text, font);
            PointF pos = new PointF(midPos.X - textSize.Width / 2, midPos.Y - textSize.Height / 2);

            // Draw back color ?
            if (backColor.HasValue)
            {
                gfx.FillRectangle(new SolidBrush(backColor.Value), pos.X, pos.Y, textSize.Width, textSize.Height);
            }

            // Draw string
            gfx.DrawString(text, font, brush, pos);
        }

        /// <summary>
        /// Get point at arc
        /// </summary>
        /// <param name="center"></param>
        /// <param name="angle"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public static Point GetPointAtArc(Point center, float angle, float radius)
        {
            float radAngle = angle * (float)Math.PI / 180.0f;

            return new Point(
                center.X + (int)((float)Math.Cos(radAngle) * radius),
                center.Y + (int)((float)Math.Sin(radAngle) * radius));
        }

        /// <summary>
        /// Draw centered circle
        /// </summary>
        /// <param name="gfx"></param>        
        public static void DrawCenterCircle(this Graphics gfx, Pen pen, PointF pos, float radius)
        {
            gfx.DrawEllipse(pen, pos.X - radius, pos.Y - radius, radius * 2.0f, radius * 2.0f);
        }
    }
}
