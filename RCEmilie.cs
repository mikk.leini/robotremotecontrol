﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO.Ports;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Media.Media3D;

namespace RobotRemoteControl
{
    /// <summary>
    /// RC Emilie (Robotex 2018 boat robot)
    /// </summary>
    public class RCEmilie : RC
    {
        private enum ReceiverState
        {
            WaitPreambula,
            WaitID,
            WaitLength,
            WaitData,
            WaitChecksum
        };

        protected enum MessageID
        {
            ControlMotors = 1,
            ControlCommand = 2,
            TextLog = 3,
            SystemData = 4,
            LidarData = 5,
            IMUOrientation = 6,
            IMULatestPose = 7
        }

        private enum LidarMeasurementStatus
        {
            NotAccessible = 0, /* Lidar chip is not accessible */
            Valid = 1,         /* Measurement is valid */
            Invalid = 2,       /* Chip is accessible but measurement failed */
            Timeout = 3        /* Measurement has timed out */
        }

        private struct LidarData
        {
            public LidarMeasurementStatus Status;
            public float Range;
        }

        private struct Vector
        {
            public float x, y, z;
        }

        private struct Orientation
        {
            public float roll, pitch, yaw;
        }

        private struct Pose
        {
            public Vector position;
            public Orientation orientation;
        }

        // Lidar status -> color map
        private readonly Dictionary<LidarMeasurementStatus, Color> LidarStatusColorMap = new Dictionary<LidarMeasurementStatus, Color>()
        {
            [LidarMeasurementStatus.Valid] = Color.DarkGreen,
            [LidarMeasurementStatus.Invalid] = Color.DarkGoldenrod,
            [LidarMeasurementStatus.Timeout] = Color.DarkGoldenrod,
            [LidarMeasurementStatus.NotAccessible] = Color.DarkRed
        };

        // Receiver variables
        private ReceiverState receiveState = ReceiverState.WaitPreambula;
        private byte receiveID, receiveLength, receiveIndex, receiveChecksum;
        private byte[] receiveData;

        // Control signals
        private bool controlActive;
        private double speedLeft, speedRight, rudder;

        // Feedback signals
        private uint uptime;
        private float batteryVoltage;
        private bool batteryIsEmpty;
        private bool buttonIsPressed;
        private float temperature;
        private LidarData[] lidarData = new LidarData[5];
        private Orientation imuOrientation;
        private List<Pose> imuTail = new List<Pose>();

        //
        // Name
        //
        override public string Name
        {
            get { return "Emilie specific drive"; }
        }

        //
        // Constructor
        //
        public RCEmilie()
        {
            if (!BitConverter.IsLittleEndian)
            {
                MessageBox.Show("Software not operating on little-endian architecture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //
        // Initialization
        //
        public override void Init()
        {
            base.Init();

            controlActive = true;
        }

        //
        // Closing
        //
        public override void Close()
        {
            base.Close();

            controlActive = false;
        }

        //
        // Command sending
        //        
        private void SendSpeedControl(sbyte left, sbyte right, sbyte rudder)
        {
            byte[] data = new byte[3];

            data[0] = (byte)left;
            data[1] = (byte)right;
            data[2] = (byte)rudder;
            
            SendMessage(MessageID.ControlMotors, data);
        }

        /// <summary>
        /// Control function
        /// </summary>
        /// <param name="values"></param>
        override public void Control(ControlValues values)
        {
            // Controlling active ?
            if (controlActive)
            {
                // Convert from X/Y to differential
                speedLeft = Limit(values.Y + values.X, -1, 1);
                speedRight = Limit(values.Y - values.X, -1, 1);

                // Z axis = rudder
                rudder = values.Z;

                // Send speed control command
                SendSpeedControl(
                    (sbyte)(speedLeft * 127),
                    (sbyte)(speedRight * 127),
                    (sbyte)(rudder * 127));

                // Space press = beep
                if ((values.Flags & 0x01) != 0)
                {
                    SendMessage(MessageID.ControlCommand, new byte[] { (byte)'b' });
                }

                // F1 press = Reset
                if ((values.Flags & 0x02) != 0)
                {
                    SendMessage(MessageID.ControlCommand, new byte[] { (byte)'r' });
                }

                // F2 press = Jump to bootloader
                if ((values.Flags & 0x04) != 0)
                {
                    SendMessage(MessageID.ControlCommand, new byte[] { (byte)'j' });

                    // Don't send any more messages because it confused ST bootloader
                    controlActive = false;
                }

                // F5 press = Start algorithm
                if ((values.Flags & 0x20) != 0)
                {
                    SendMessage(MessageID.ControlCommand, new byte[] { (byte)'s' }); 
                }

                // F6 press = Stop algorithm
                if ((values.Flags & 0x40) != 0)
                {
                    SendMessage(MessageID.ControlCommand, new byte[] { (byte)'p' });
                }
            }
        }

        /// <summary>
        /// Painting
        /// </summary>
        /// <param name="gfx"></param>
        /// <param name="quad"></param>
        override public void Paint(Graphics gfx, Point quad)
        {
            int sl = (int)(speedLeft * quad.Y);
            int sr = (int)(speedRight * quad.Y);
            int rp = (int)(rudder * quad.X);

            // Draw boat
            Image paat = global::RobotRemoteControl.Properties.Resources.paat;
            gfx.DrawImage(paat, -paat.Width / 2, -paat.Height / 2);

            // Motor control bars
            gfx.DrawLine(new Pen(CreateSpeedColor(speedLeft), 5), new Point(-quad.X + 3, 0), new Point(-quad.X + 3, -sl));
            gfx.DrawLine(new Pen(CreateSpeedColor(speedRight), 5), new Point(quad.X - 3, 0), new Point(quad.X - 3, -sr));
            gfx.DrawLine(new Pen(CreateSpeedColor(rudder, true), 5), new Point(0, quad.Y - 3), new Point(rp, quad.Y - 3));

            // Show uptime and battery status
            gfx.DrawString("UP: " + (uptime / 1000.0).ToString("F1") + " s", SystemFonts.DefaultFont, Brushes.Black, -quad.X + 10, -quad.Y + 5);
            gfx.DrawString("BAT: " + batteryVoltage.ToString("F2") + " V", SystemFonts.DefaultFont, (batteryIsEmpty ? Brushes.Red : Brushes.Black), -quad.X + 10, -quad.Y + 20);

            // Show lidars
            DrawLidarBeam(gfx, new Point(-25, -05), -90, lidarData[0]);
            DrawLidarBeam(gfx, new Point(-09, -12), -30, lidarData[1]);
            DrawLidarBeam(gfx, new Point(+00, -15), +00, lidarData[2]);
            DrawLidarBeam(gfx, new Point(+09, -12), +30, lidarData[3]);
            DrawLidarBeam(gfx, new Point(+15, -05), +90, lidarData[4]);

            // Draw IMU
            DrawIMUGauge(gfx, new Point(quad.X - 040, -quad.Y + 45), imuOrientation.roll,  90, "Roll",  Color.Green);
            DrawIMUGauge(gfx, new Point(quad.X - 100, -quad.Y + 45), imuOrientation.pitch, 90, "Pitch", Color.Blue);
            DrawIMUGauge(gfx, new Point(quad.X - 160, -quad.Y + 45), imuOrientation.yaw,   0,  "Yaw",   Color.Gold);

            // Draw tail
            DrawIMUTail(gfx);
        }

        /// <summary>
        /// Draw lidar beam
        /// </summary>
        /// <param name="gfx"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="data"></param>
        private void DrawLidarBeam(Graphics gfx, Point start, float angle, LidarData data)
        {
            Point lineEnd = GraphicsExtension.GetPointAtArc(start, angle + 270, (data.Range / 1.350f) * 300.0f);
            Point textPos = GraphicsExtension.GetPointAtArc(start, angle + 270, 60.0f);
            Color col = LidarStatusColorMap[data.Status];            

            gfx.DrawLine(new Pen(col, 1), start, lineEnd);
            gfx.DrawCenterString((data.Range * 100).ToString("F1") + " cm", SystemFonts.DefaultFont, new SolidBrush(col), textPos, SystemColors.Control);
        }

        /// <summary>
        /// Draw IMU gauge
        /// </summary>
        /// <param name="gfx"></param>
        /// <param name="pos"></param>
        /// <param name="angle"></param>
        /// <param name="title"></param>
        /// <param name="color"></param>
        private void DrawIMUGauge(Graphics gfx, Point pos, float angle, float angleOffset, string title, Color color)
        {
            int radius = 25;
            angle += 270 + angleOffset;

            PointF p1  = GraphicsExtension.GetPointAtArc(pos, angle, radius);
            PointF p2  = GraphicsExtension.GetPointAtArc(pos, angle, -radius);
            PointF p1A = GraphicsExtension.GetPointAtArc(pos, angle + 20, radius - 12);
            PointF p1B = GraphicsExtension.GetPointAtArc(pos, angle - 20, radius - 12);

            // Background
            gfx.FillEllipse(Brushes.GhostWhite, pos.X - 25, pos.Y - 25, 50, 50);

            // Arrow
            Pen linePen = new Pen(color, 3);
            gfx.DrawLine(linePen, p1, p2);
            gfx.DrawLine(linePen, p1, p1A);
            gfx.DrawLine(linePen, p1, p1B);

            // Border
            gfx.DrawEllipse(Pens.Gray, pos.X - 25, pos.Y - 25, 50, 50);

            // Title
            gfx.DrawCenterString(title, SystemFonts.DefaultFont, Brushes.Gray, new Point(pos.X, pos.Y - 34));
        }

        /// <summary>
        /// Draw IMU tail
        /// </summary>
        /// <param name="gfx"></param>
        private void DrawIMUTail(Graphics gfx)
        {
            float scale = 100.0f; // meters to centimerers
            Vector3D lastPoint = new Vector3D(0, 0, 0);

            foreach (Pose pose in imuTail.Reverse<Pose>())
            {
                Vector3D newPoint = new Vector3D(pose.position.x * scale, pose.position.y * scale, pose.position.z * scale);
                newPoint += lastPoint;

                // Draw line between points
                gfx.DrawLine(Pens.Black, (float)lastPoint.X, (float)lastPoint.Y, (float)newPoint.X, (float)newPoint.Y);

                lastPoint = newPoint;
            }
        }

        /// <summary>
        /// Assemble and send message
        /// </summary>
        /// <param name="id"></param>
        /// <param name="data"></param>
        protected void SendMessage(MessageID id, byte[] data)
        {
            SendMessage((byte)id, data);
        }

        /// <summary>
        /// Assemble and send message
        /// </summary>
        /// <param name="id"></param>
        /// <param name="length"></param>
        /// <param name="data"></param>
        protected void SendMessage(byte id, byte[] data)
        {
            byte[] buffer = new byte[data.Length + 4];
            byte checksum = 0;

            // Form packet
            buffer[0] = 0xAA;
            buffer[1] = id;
            buffer[2] = (byte)data.Length;

            // Calculate header checksum
            checksum += buffer[0];
            checksum += buffer[1];
            checksum += buffer[2];
            
            for (int i = 0; i < data.Length; i++)
            {
                buffer[3 + i] = data[i];
                checksum += data[i];
            }

            // Checksum
            buffer[3 + data.Length] = (byte)(checksum & 0xFF);

            // Write to send buffer
            Send(buffer, 4 + data.Length);

            //LogMessage(BinaryToHexString(buffer, 4 + length));
        }

        /// <summary>
        /// Data reception event
        /// </summary>
        /// <param name="buffer"></param>
        override public void DataReceived(byte[] buffer)
        {
            // Parse all bytes
            foreach (byte b in buffer)
            {
                switch (receiveState)
                {
                    case ReceiverState.WaitPreambula:
                        if (b == 0xAA)
                        {
                            receiveState = ReceiverState.WaitID;
                            receiveIndex = 0;
                            receiveChecksum = b;
                        }
                        else
                        {
                            // Unexpected byte...
                        }
                        break;

                    case ReceiverState.WaitID:
                        receiveID = b;
                        receiveChecksum += b;
                        receiveState = ReceiverState.WaitLength;
                        break;

                    case ReceiverState.WaitLength:
                        receiveLength = b;
                        receiveData = new byte[receiveLength];
                        receiveIndex = 0;
                        receiveChecksum += b;
                        receiveState = ReceiverState.WaitData;
                        break;

                    case ReceiverState.WaitData:
                        receiveData[receiveIndex++] = b;
                        receiveChecksum += b;

                        // Got last byte ?
                        if (receiveIndex >= receiveLength)
                        {
                            receiveState = ReceiverState.WaitChecksum;
                        }
                        break;

                    case ReceiverState.WaitChecksum:

                        // Check checksum
                        if (b == receiveChecksum)
                        {
                            // Correct message
                            HandleMessage(receiveID, receiveData);
                        }

                        receiveState = ReceiverState.WaitPreambula;
                        break;
                }
            }
        }

        /// <summary>
        /// Received message handler
        /// </summary>
        /// <param name="id"></param>
        /// <param name="buffer"></param>
        protected void HandleMessage(byte id, byte[] data)
        {
            switch (id)
            {
                // Text
                case (byte)MessageID.TextLog:
                    LogMessage(Encoding.Default.GetString(data));
                    break;

                // System data
                case (byte)MessageID.SystemData:
                    if (data.Length >= 8)
                    {
                        uptime = BitConverter.ToUInt32(data, 0);
                        batteryVoltage = (float)BitConverter.ToUInt16(data, 4) / 1000.0f; // mV to V
                        temperature = (float)data[6];
                        batteryIsEmpty = ((data[7] & 0x01) != 0);
                        buttonIsPressed = ((data[7] & 0x02) != 0);
                    }
                    break;

                // Lidar data
                case (byte)MessageID.LidarData:
                    if (data.Length >= 1)
                    {
                        byte lidarCount = Math.Min(data[0], (byte)lidarData.Length);
                        
                        for (int l = 0; l < lidarCount; l++)
                        {
                            lidarData[l].Status = (LidarMeasurementStatus)data[1 + l * 3];
                            lidarData[l].Range = (float)BitConverter.ToUInt16(data, 2 + l * 3) / 1000.0f; // mm to m
                        }
                    }
                    break;

                // IMU orientation
                case (byte)MessageID.IMUOrientation:
                    if (data.Length == 6)
                    {
                        imuOrientation.roll  = BitConverter.ToUInt16(data, 0);
                        imuOrientation.pitch = BitConverter.ToUInt16(data, 2);
                        imuOrientation.yaw   = BitConverter.ToUInt16(data, 4);
                    }
                    break;

                // IMU latest pose
                case (byte)MessageID.IMULatestPose:
                    if (data.Length == 24)
                    {
                        Pose p;

                        p.position.x = BitConverter.ToSingle(data, 0);
                        p.position.y = BitConverter.ToSingle(data, 4);
                        p.position.z = BitConverter.ToSingle(data, 8);

                        p.orientation.roll  = BitConverter.ToSingle(data, 12);
                        p.orientation.pitch = BitConverter.ToSingle(data, 16);
                        p.orientation.yaw   = BitConverter.ToSingle(data, 20);

                        imuTail.Add(p);
                        
                        // Remove very old tail points
                        while (imuTail.Count > 100)
                        {
                            imuTail.RemoveAt(0);
                        }
                    }
                    break;

                // Something else
                default:
                    LogMessage(id.ToString() + " = " + BitConverter.ToString(data, 0, data.Length));
                    break;
            }
        }
    }
}
