﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO.Ports;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Media.Media3D;

namespace RobotRemoteControl
{
    /// <summary>
    /// RC AceVentura 2
    /// </summary>
    public class RCAceVentura2 : RC
    {
        private const double maxRPM = 700;

        private enum ReceiverState
        {
            WaitPreambula,
            WaitID,
            WaitLength,
            WaitData,
            WaitChecksum
        };

        protected enum MessageID
        {
            Text = 0,
            ControlSpeed = 1,
            FeedbackOdometry = 2,
            ControlLED = 3,
            ButtonPressEvent = 4,
            ControlLidarMotor = 5,
            FeedbackSystem = 6,
            Ping = 7,
            Pong = 8,            
        }

        // Receiver variables
        private ReceiverState receiveState = ReceiverState.WaitPreambula;
        private byte receiveID, receiveLength, receiveIndex, receiveChecksum;
        private byte[] receiveData;

        // Control signals
        private bool controlActive;
        private bool activateLED;
        private double[] motorControlSpeed = new double[3];

        // Feedback signals
        private uint uptime;
        private double batteryVoltage;
        private double temperature;
        private int[] motorPulses = new int[3];
        private double[] motorActualSpeed = new double[3];

        //
        // Name
        //
        override public string Name
        {
            get { return "Ace Ventura 2"; }
        }

        //
        // Constructor
        //
        public RCAceVentura2()
        {
            if (!BitConverter.IsLittleEndian)
            {
                MessageBox.Show("Software not operating on little-endian architecture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //
        // Initialization
        //
        public override void Init()
        {
            base.Init();

            controlActive = true;
        }

        //
        // Closing
        //
        public override void Close()
        {
            base.Close();

            controlActive = false;
        }

        /// <summary>
        /// Control function
        /// </summary>
        /// <param name="values"></param>
        override public void Control(ControlValues values)
        {
            // Controlling active ?
            if (controlActive)
            {
                // Convert from X/Y/Z to differential
                motorControlSpeed[0] = 2.0 * values.X - values.Z;
                motorControlSpeed[1] = -values.Y - values.X - values.Z;
                motorControlSpeed[2] = +values.Y - values.X - values.Z;

                // Limit values
                motorControlSpeed[0] = Limit(motorControlSpeed[0], -1, 1);
                motorControlSpeed[1] = Limit(motorControlSpeed[1], -1, 1);
                motorControlSpeed[2] = Limit(motorControlSpeed[2], -1, 1);

                // Send speed control command
                SendControlSpeed(
                    (short)(motorControlSpeed[0] * maxRPM),
                    (short)(motorControlSpeed[1] * maxRPM),
                    (short)(motorControlSpeed[2] * maxRPM));

                // Activate LED ?
                activateLED = (values.Flags & 1) != 0;
                SendControlLED(0, activateLED ? Color.DeepPink : Color.Black);

                // Activate lidar ?
                if ((values.Flags & 2) != 0)
                {
                    SendMessage(MessageID.ControlLidarMotor, new byte[] { 20 });
                }
            }
        }

        /// <summary>
        /// Painting
        /// </summary>
        /// <param name="gfx"></param>
        /// <param name="quad"></param>
        override public void Paint(Graphics gfx, Point quad)
        {
            float radius = Math.Min(quad.X, quad.Y) * 0.8f;

            // Draw control bars
            DrawSpeedStatus(gfx, radius - 2, 180, (float)motorControlSpeed[0]);
            DrawSpeedStatus(gfx, radius - 2, -60, (float)motorControlSpeed[1]);
            DrawSpeedStatus(gfx, radius - 2, +60, (float)motorControlSpeed[2]);

            // Draw status bars
            DrawSpeedStatus(gfx, radius + 2, 180, (float)motorActualSpeed[0]);
            DrawSpeedStatus(gfx, radius + 2, -60, (float)motorActualSpeed[1]);
            DrawSpeedStatus(gfx, radius + 2, +60, (float)motorActualSpeed[2]);

            // Show uptime and battery status
            gfx.DrawString("UP: " + (uptime / 1000.0).ToString("F1") + " s", SystemFonts.DefaultFont, Brushes.Black, -quad.X + 10, -quad.Y + 5);
            gfx.DrawString("BAT: " + batteryVoltage.ToString("F2") + " V", SystemFonts.DefaultFont, Brushes.Black, -quad.X + 10, -quad.Y + 20);
            gfx.DrawString("Temp: " + temperature.ToString("F1") + " C", SystemFonts.DefaultFont, Brushes.Black, -quad.X + 10, -quad.Y + 35);

            // Show pulses (odometry)
            gfx.DrawString(motorPulses[0].ToString(), SystemFonts.DefaultFont, Brushes.Black, new Point(0, 30));
            gfx.DrawString(motorPulses[1].ToString(), SystemFonts.DefaultFont, Brushes.Black, new Point(-60, -30));
            gfx.DrawString(motorPulses[2].ToString(), SystemFonts.DefaultFont, Brushes.Black, new Point(+60, -30));
        }

        /// <summary>
        /// Draw speed status
        /// </summary>
        /// <param name="gfx"></param>
        /// <param name="radius"></param>
        /// <param name="angle"></param>
        /// <param name="speed"></param>
        private void DrawSpeedStatus(Graphics gfx, float radius, float angle, float speed)
        {
            // Decide color
            int cs = 127 + (int)(speed * 255);
            cs = Math.Min(255, Math.Max(0, cs));
            Color arcColor = Color.FromArgb(255, 255 - cs, 0, cs);

            // Set pen
            Pen arcPen = new Pen(arcColor, 3);

            // Draw arc
            try
            {
                gfx.DrawArc(arcPen,
                    -radius, -radius,
                    radius * 2, radius * 2,
                    angle - 90, speed * 30);
            }
            catch (Exception _)
            {

            }
        }

        /// <summary>
        /// Assemble and send message
        /// </summary>
        /// <param name="id"></param>
        /// <param name="data"></param>
        protected void SendMessage(MessageID id, byte[] data)
        {
            SendMessage((byte)id, data);
        }

        /// <summary>
        /// Assemble and send message
        /// </summary>
        /// <param name="id"></param>
        /// <param name="length"></param>
        /// <param name="data"></param>
        protected void SendMessage(byte id, byte[] data)
        {
            byte[] buffer = new byte[data.Length + 4];
            byte checksum = 0;

            // Form packet
            buffer[0] = 0xAA;
            buffer[1] = id;
            buffer[2] = (byte)data.Length;

            // Calculate header checksum
            checksum += buffer[0];
            checksum += buffer[1];
            checksum += buffer[2];

            for (int i = 0; i < data.Length; i++)
            {
                buffer[3 + i] = data[i];
                checksum += data[i];
            }

            // Checksum
            buffer[3 + data.Length] = (byte)(checksum & 0xFF);

            // Write to send buffer
            Send(buffer, 4 + data.Length);

            //LogMessage(BinaryToHexString(buffer, 4 + length));
        }

        /// <summary>
        /// Send speed control message
        /// </summary>
        /// <param name="m1">Motor 1 speed in RPM</param>
        /// <param name="m2">Motor 2 speed in RPM</param>
        /// <param name="m3">Motor 3 speed in RPM</param>
        private void SendControlSpeed(short m1, short m2, short m3)
        {            
            byte[] data = new byte[6];

            BitConverter.GetBytes(m1).CopyTo(data, 0);
            BitConverter.GetBytes(m2).CopyTo(data, 2);
            BitConverter.GetBytes(m3).CopyTo(data, 4);

            SendMessage(MessageID.ControlSpeed, data);
        }

        /// <summary>
        /// Send LED control message
        /// </summary>
        /// <param name="led">LED index (from 0)</param>
        /// <param name="color">LED color</param>
        private void SendControlLED(byte led, Color color)
        {
            byte[] data = new byte[4];

            data[0] = led;            
            data[1] = color.B;
            data[2] = color.G;
            data[3] = color.R;

            SendMessage(MessageID.ControlLED, data);
        }

        /// <summary>
        /// Data reception event
        /// </summary>
        /// <param name="buffer"></param>
        override public void DataReceived(byte[] buffer)
        {
            // Parse all bytes
            foreach (byte b in buffer)
            {
                switch (receiveState)
                {
                    case ReceiverState.WaitPreambula:
                        if (b == 0xAA)
                        {
                            receiveState = ReceiverState.WaitID;
                            receiveIndex = 0;
                            receiveChecksum = b;
                        }
                        else
                        {
                            // Unexpected byte...
                        }
                        break;

                    case ReceiverState.WaitID:
                        receiveID = b;
                        receiveChecksum += b;
                        receiveState = ReceiverState.WaitLength;
                        break;

                    case ReceiverState.WaitLength:
                        receiveLength = b;
                        receiveData = new byte[receiveLength];
                        receiveIndex = 0;
                        receiveChecksum += b;
                        receiveState = ReceiverState.WaitData;
                        break;

                    case ReceiverState.WaitData:
                        receiveData[receiveIndex++] = b;
                        receiveChecksum += b;

                        // Got last byte ?
                        if (receiveIndex >= receiveLength)
                        {
                            receiveState = ReceiverState.WaitChecksum;
                        }
                        break;

                    case ReceiverState.WaitChecksum:

                        // Check checksum
                        if (b == receiveChecksum)
                        {
                            // Correct message
                            HandleMessage(receiveID, receiveData);
                        }

                        receiveState = ReceiverState.WaitPreambula;
                        break;
                }
            }
        }

        /// <summary>
        /// Received message handler
        /// </summary>
        /// <param name="id"></param>
        /// <param name="buffer"></param>
        protected void HandleMessage(byte id, byte[] data)
        {
            switch (id)
            {
                // Text
                case (byte)MessageID.Text:
                    LogMessage(Encoding.Default.GetString(data));
                    break;

                // System data
                case (byte)MessageID.FeedbackSystem:
                    if (data.Length >= 8)
                    {
                        uptime = BitConverter.ToUInt32(data, 0);
                        batteryVoltage = (double)BitConverter.ToUInt16(data, 4) / 1000.0f; // mV to V
                        temperature = (double)data[6];
                    }
                    break;

                // Motors odometry
                case (byte)MessageID.FeedbackOdometry:
                    if (data.Length == 8)
                    {
                        motorPulses[0] = BitConverter.ToUInt16(data, 0);
                        motorPulses[1] = BitConverter.ToUInt16(data, 2);
                        motorPulses[2] = BitConverter.ToUInt16(data, 4);
                        // Byte 5-6: timestamp
                    }
                    break;

                // Button press even
                case (byte)MessageID.ButtonPressEvent:
                    if (data.Length == 2)
                    {
                        LogMessage($"Button {data[0]}" + (data[1] == 1 ? " repeat" : string.Empty));
                    }
                    break;

                // Something else
                default:
                    LogMessage(id.ToString() + " = " + BitConverter.ToString(data, 0, data.Length));
                    break;
            }
        }
    }
}
