﻿namespace RobotRemoteControl
{
    public struct ControlValues
	{
        public double X, Y, Z;
        public double Acceleration;
        public int Flags;
	}
}
