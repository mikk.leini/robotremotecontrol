﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO.Ports;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Media.Media3D;

namespace RobotRemoteControl
{
    /// <summary>
    /// RC Ace Ventura
    /// </summary>
    public class RCAceVentura : RC
    {
        private enum ReceiverState
        {
            WaitPreambula,
            WaitID,
            WaitLength,
            WaitData,
            WaitChecksum
        };

        protected enum MessageID
        {
            MessageControlSpeed = 1,
            MessageFeedbackOdometry = 2
        }

        // Receiver variables
        private ReceiverState receiveState = ReceiverState.WaitPreambula;
        private byte receiveID, receiveLength, receiveIndex, receiveChecksum;
        private byte[] receiveData;

        // Control signals
        private bool controlActive;
        private double speedLeft, speedRight, rudder;

        // Feedback signals
        private long[] absPulses = new long[4];
        private sbyte[] prevPulses = new sbyte[4];
        private long lastPulseTime;
        private double[] realSpeed = new double[4];

        //
        // Name
        //
        override public string Name
        {
            get { return "Ace Ventura"; }
        }

        //
        // Constructor
        //
        public RCAceVentura()
        {
            if (!BitConverter.IsLittleEndian)
            {
                MessageBox.Show("Software not operating on little-endian architecture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //
        // Initialization
        //
        public override void Init()
        {
            base.Init();

            controlActive = true;
        }

        //
        // Closing
        //
        public override void Close()
        {
            base.Close();

            controlActive = false;
        }

        /// <summary>
        /// Control function
        /// </summary>
        /// <param name="values"></param>
        override public void Control(ControlValues values)
        {
            // Controlling active ?
            if (controlActive)
            {
                // Convert from X/Y to differential
                speedLeft = Limit(values.Y + values.X, -1, 1);
                speedRight = Limit(values.Y - values.X, -1, 1);

                // Z axis = rudder
                rudder = values.Z;

                double maxSpeed = 127;

                // Send speed control command
                SendControlSpeed(
                    (sbyte)((speedRight + rudder) *  maxSpeed),   // RR
                    (sbyte)((speedLeft  - rudder) * -maxSpeed),   // RL
                    (sbyte)((speedRight - rudder) *  maxSpeed),   // FR
                    (sbyte)((speedLeft  + rudder) * -maxSpeed));  // FL

                //SendControlSpeed(0, 0, (sbyte)(speedLeft * maxSpeed), 0);
            }
        }

        /// <summary>
        /// Painting
        /// </summary>
        /// <param name="gfx"></param>
        /// <param name="quad"></param>
        override public void Paint(Graphics gfx, Point quad)
        {
            int sl = (int)(speedLeft * quad.Y);
            int sr = (int)(speedRight * quad.Y);
            int rp = (int)(rudder * quad.X);

            // Motor control bars
            gfx.DrawLine(new Pen(CreateSpeedColor(speedLeft), 5), new Point(-quad.X + 3, 0), new Point(-quad.X + 3, -sl));
            gfx.DrawLine(new Pen(CreateSpeedColor(speedRight), 5), new Point(quad.X - 3, 0), new Point(quad.X - 3, -sr));
            gfx.DrawLine(new Pen(CreateSpeedColor(rudder, true), 5), new Point(0, quad.Y - 3), new Point(rp, quad.Y - 3));

            for (int i = 0; i < 4; i++)
            {
                gfx.DrawString(absPulses[i].ToString(), SystemFonts.DefaultFont, Brushes.Black, new Point(-100, i * 15));
                gfx.DrawString(realSpeed[i].ToString("0.00"), SystemFonts.DefaultFont, Brushes.Black, new Point(+100, i * 15));
            }
        }


        /// <summary>
        /// Assemble and send message
        /// </summary>
        /// <param name="id"></param>
        /// <param name="data"></param>
        protected void SendMessage(MessageID id, byte[] data)
        {
            SendMessage((byte)id, data);
        }

        /// <summary>
        /// Assemble and send message
        /// </summary>
        /// <param name="id"></param>
        /// <param name="length"></param>
        /// <param name="data"></param>
        protected void SendMessage(byte id, byte[] data)
        {
            byte[] buffer = new byte[data.Length + 4];
            byte checksum = 0;

            // Form packet
            buffer[0] = 0xAA;
            buffer[1] = id;
            buffer[2] = (byte)data.Length;

            // Calculate header checksum
            checksum += buffer[0];
            checksum += buffer[1];
            checksum += buffer[2];

            for (int i = 0; i < data.Length; i++)
            {
                buffer[3 + i] = data[i];
                checksum += data[i];
            }

            // Checksum
            buffer[3 + data.Length] = (byte)(checksum & 0xFF);

            // Write to send buffer
            Send(buffer, 4 + data.Length);

            //LogMessage(BinaryToHexString(buffer, 4 + length));
        }

        /// <summary>
        /// Send speed control message
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <param name="s3"></param>
        /// <param name="s4"></param>
        private void SendControlSpeed(sbyte s1, sbyte s2, sbyte s3, sbyte s4)
        {            
            byte[] data = new byte[4];

            data[0] = (byte)s1;
            data[1] = (byte)s2;
            data[2] = (byte)s3;
            data[3] = (byte)s4;
            
            SendMessage(MessageID.MessageControlSpeed, data);
        }

        /// <summary>
        /// Data reception event
        /// </summary>
        /// <param name="buffer"></param>
        override public void DataReceived(byte[] buffer)
        {
            // Parse all bytes
            foreach (byte b in buffer)
            {
                switch (receiveState)
                {
                    case ReceiverState.WaitPreambula:
                        if (b == 0xAA)
                        {
                            receiveState = ReceiverState.WaitID;
                            receiveIndex = 0;
                            receiveChecksum = b;
                        }
                        else
                        {
                            // Unexpected byte...
                        }
                        break;

                    case ReceiverState.WaitID:
                        receiveID = b;
                        receiveChecksum += b;
                        receiveState = ReceiverState.WaitLength;
                        break;

                    case ReceiverState.WaitLength:
                        receiveLength = b;
                        receiveData = new byte[receiveLength];
                        receiveIndex = 0;
                        receiveChecksum += b;
                        receiveState = ReceiverState.WaitData;
                        break;

                    case ReceiverState.WaitData:
                        receiveData[receiveIndex++] = b;
                        receiveChecksum += b;

                        // Got last byte ?
                        if (receiveIndex >= receiveLength)
                        {
                            receiveState = ReceiverState.WaitChecksum;
                        }
                        break;

                    case ReceiverState.WaitChecksum:

                        // Check checksum
                        if (b == receiveChecksum)
                        {
                            // Correct message
                            HandleMessage(receiveID, receiveData);
                        }

                        receiveState = ReceiverState.WaitPreambula;
                        break;
                }
            }
        }

        /// <summary>
        /// Received message handler
        /// </summary>
        /// <param name="id"></param>
        /// <param name="buffer"></param>
        protected void HandleMessage(byte id, byte[] data)
        {
            switch (id)
            {
                // Text
                case (byte)MessageID.MessageFeedbackOdometry:
                    if (data.Length >= 4)
                    {
                        long now = DateTime.Now.Ticks;
                        double timeDiff = (double)(now - lastPulseTime) * 0.0000001;
                        lastPulseTime = now;                      

                        for (int i = 0; i < 4; i++)
                        {
                            sbyte p = (sbyte)data[i];
                            int pulseDiff = (int)p - (int)prevPulses[i];
                            prevPulses[i] = p;

                            // Under or overflow ?
                            if (Math.Abs(pulseDiff) > 127)
                            {
                                if (p > 0)
                                {
                                    pulseDiff = pulseDiff - 256;
                                }
                                else
                                {
                                    pulseDiff = 256 + pulseDiff;
                                }
                            }

                            absPulses[i] += pulseDiff;

                            // Calculate speed if it's time with some filter
                            if (timeDiff > 0)
                            {
                                double speed = pulseDiff / timeDiff;
                                realSpeed[i] = (realSpeed[i] * 10 + speed) / 11;
                            }
                        }
                    }
                    break;

                // Something else
                default:
                    LogMessage(id.ToString() + " = " + BitConverter.ToString(data, 0, data.Length));
                    break;
            }
        }
    }
}
